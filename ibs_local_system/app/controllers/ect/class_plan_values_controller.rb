class Ect::ClassPlanValuesController < ApplicationController

  def show
    #index
    @engagement_id = params[:engagement_id].to_s
    @plan_version_id = -1

    #更新出来る
    @bk_flag = false

    #遷移先変更
    @redirect_url = ect_engagement_class_plan_values_path(:engagement_id => @engagement_id, :plan_version_id => @plan_version_id)
    @engagement_detail = ect_engagement_path(@engagement_id)

    #Excelに出すかどうか
    @button = params[:button].to_s
    if @button == 'Export'
      #Excel出力する
    end

    #上のリスト
    @engagements = IbsEngagements.all
    @engagements = IbsEngagements.where(:engagement_id => @engagement_id)


    #下のリスト
    # ibs_local_job_levels    役職総合
    # に含む
    # ect_class_plan_values   その会社の役職データ
    # にある
    # ect_class_actual_values 月のデータ
    # に設定が入るかどうか判定
    #

  end


  def edit
    #edit画面

    @engagement_id = params[:engagement_id].to_s


    #呼ぶテーブル
    #ect_class_plan_values
    #ect_bk_class_plan_values
    #上のリスト
    @engagements = IbsEngagements.all
    @engagements = IbsEngagements.where(:engagement_id => @engagement_id)


    #下のリスト
    # ibs_local_job_levels    役職総合
    # に含む
    # ect_class_plan_values   その会社の役職データ
    # にある
    # ect_class_actual_values 月のデータ
    # に設定が入るかどうか判定
    #

  end


  def update
    #update処理
    @engagement_id = params[:engagement_id].to_s

    #呼ぶテーブル
    #ect_class_plan_values
    #ect_bk_class_plan_values
    #上のリスト
    @engagements = IbsEngagements.all
    @engagements = IbsEngagements.where(:engagement_id => @engagement_id)



  end

end