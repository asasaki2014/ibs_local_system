class Ect::AjaxGetDataController < ApplicationController

  #assets/javascripts/application.jsから呼ばれる
  #Engagement一覧画面（検索用）
  def get_profile_center

    #選んだコード
    @company_code = params['company_code'].to_i

    #検索
    @profile_centers = IbsProfitCenters.where(:profit_center => @company_code).order(:profit_center)

    json = {}
    @count = 0
    @profile_centers.each do |pc|
      json[i] = {}
      json[i]['value'] = pc.profit_center.to_s             #ID取得
      json[i]['name']  = pc.profit_center_name_long.to_s   #表示する名前を入れておく
      @count += 1
    end

    #仮のパラメータ
    if @count == 0
      for i in 0..5 do
        json[i] = {}
        json[i]['value'] = i      #ID取得
        json[i]['name']  = "ProfileCenter=#{i.to_s}_CompanyCode=#{@company_code.to_s}"   #表示する名前を入れておく
      end
    end

    #to_jsonで変換する
    render :text => json.to_json, :type=> "text/plain"
    return
  end

end
