class Ect::EngagementsController < ApplicationController
  before_action :set_engagement, only: [:show, :edit, :update, :destroy]

  # GET /engagements
  # GET /engagements.json
  def index

    #自分用か他人様か
    @my = params[:my].to_i

    #データ検索
    @engagements = IbsEngagements.all

    #検索条件
    @button = params[:button]
    if @button == 'Search' || @button == "Export"
      @selCompanyCode = params[:selCompanyCode]
      @ep_name = params[:ep_name].to_s.strip
      @em_name = params[:em_name].to_s.strip
      #検索条件追加
      @engagements = @engagements.where(:ibs_company_id => @selCompanyCode) unless @selCompanyCode.nil?
      @engagements = @engagements.where("ep_name LIKE '%#{@ep_name}%'") unless @ep_name.nil?
      @engagements = @engagements.where("em_name LIKE '%#{@em_name}%'") unless @em_name.nil?
      logger.debug "@ep_name=#{@ep_name}"
      logger.debug "@em_name=#{@em_name}"
    else
      #ない場合
    end

    #自分だけかどうか判定
    if @my == 1

    end

    #検索
    @engagements = @engagements.page(params[:page]).per(10).order('id DESC')
    #total
    @total_count = @engagements.count

    #Exportの場合はパラメータ渡して出力も行う
    if @button == "Export"
      excel(@engagements)
    end


    #呼ぶテーブル
    #ibs_engagements(LeadとSubの合計に注意)
    #ibs_team_members.rb(権限によってはチームメンバーのデータが見える)
    #ibs_companies.rb(上のリスト)
    #ibs_profit_centers.rb(上のリスト)

  end

  #エクセル出力テスト2(採用)
  def excel(params)
    @date_from = Date.new(2014,3,1)
    @date_to = Date.new(2014,3,31)
    @product = nil #Product.find(params[:id])
    @stocks = nil #ProductStock
      #.where(product_id: @product.id)
      #.where(date: @date_from..@date_to)
      #.order(:date)

    package = Axlsx::Package.new
    workbook = package.workbook
    workbook.styles.fonts.first.name = 'MS Pゴシック'
    workbook.styles do |style|
      title_cell = style.add_style sz: 16
      table_header_cell = style.add_style :bg_color => 'AAAAAA',
                                          :fg_color => 'FFFFFF',
                                          :border => { style: :thin, color: '00' },
                                          #:parse_fill_options => 3,
                                          #:type => :dxf,
                                          #:PatternFill => patternType => 'solid'
                                          :alignment => {:indent => 1}

      table_cell = style.add_style border: { style: :thin, color: '00' },
                                                 alignment: { horizontal: :left }

      workbook.add_worksheet(name: '在庫一覧') do |sheet|
        sheet.add_row ["在庫推移"], style: title_cell
        sheet.column_info.first.width = 5 # 1列目の幅は小さく

        sheet.add_row []

        product_head_table_styles = [nil, table_header_cell, table_cell, table_cell, table_cell]
        sheet.add_row ['', '商品名', "名前", '', ''], style: product_head_table_styles
        sheet.add_row ['', '商品コード', "商品コード", '', ''], style: product_head_table_styles
        sheet.add_row ['', '在庫数', 10, '', ''], style: product_head_table_styles
        sheet.add_row ['', '在庫数', 15, '', ''], style: product_head_table_styles
        sheet.add_row ['', '在庫数', "=sum(C5,C6)", '', ''], style: product_head_table_styles

        sheet.merge_cells("C3:E3")
        sheet.merge_cells("C4:E4")
        sheet.merge_cells("C5:E5")
        sheet.merge_cells("C6:E6")

        #グラフ的なことも出せるらしい
        #sheet.add_row []
        #sheet.add_row []

        #sheet.add_row ['', '日時', '商品コード', '在庫数'],
        #  style: [nil, table_header_cell,table_header_cell,table_header_cell]
        #@stocks.each do |stock|
        #  sheet.add_row ['', stock.date.to_s, @product.sku, stock.quantity],
        #    style: [nil, table_cell, table_cell, table_cell]
        #end
        #sheet.add_chart(
        #  Axlsx::LineChart, title: "在庫推移", rotX: 30, rotY: 20) do |chart|
        #   chart.start_at 6, 3
        #   chart.end_at 12, 20
        #   #chart.add_series data: sheet["D9:D#8"],
        #   #                #  labels: sheet["B9:B#{8 + @stocks.count}"], # 横軸を指定できる
        #   #                 title: '在庫数',
        #   #                 color: '00FF00'
        #   chart.catAxis.title = '日時'
        # end
      end
    end

    begin
      file = Tempfile.new('zaiko.xlsx', "#{Rails.root.to_s}/tmp/")
      package.serialize file.path
      send_file file.path,
        filename: "posts.xlsx",
        type:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    ensure
      file.close
    end
  end


  # GET /engagements/1
  # GET /engagements/1.json
  def show
  end

end
