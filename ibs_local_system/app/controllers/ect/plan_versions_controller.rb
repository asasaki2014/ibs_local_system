class Ect::PlanVersionsController < ApplicationController

  #過去のバージョン一覧表示
  def index
    #index
    @engagement_id = params[:engagement_id].to_s

    #呼ぶテーブル
    #ect_class_plan_values
    #ect_bk_class_plan_values
    #過去バージョンのデータを取得する
    @engagements = EctPlanVersions.all
    @engagements = @engagements.where(:engagement_id => @engagement_id)

  end

  #過去バージョン一覧から表示取得する
  def class_plan_values
    #class_plan_values
    @engagement_id = params[:engagement_id].to_s
    @plan_version_id = params[:plan_version_id].to_s

    #更新できない
    @bk_flag = true

    #遷移先（過去指定）
    @redirect_url = ect_engagement_plan_version_class_plan_values_path(:engagement_id => @engagement_id, :plan_version_id => @plan_version_id)
    @engagement_detail = ect_engagement_plan_version_show_path(:engagement_id => @engagement_id, :plan_version_id => @plan_version_id)

    #過去バージョンのデータを取得する
    @engagements = EctBkEngagements.all
    @engagements = @engagements.where(:engagement_id => @engagement_id)
    @engagements = @engagements.where(:plan_version_id => @plan_version_id)

    #Excelに出すかどうか
    @button = params[:button].to_s
    if @button == 'Export'
      #Excel出力する
    end

    #前のshowを使う
    render :template => "ect/class_plan_values/show"


    #呼ぶテーブル
    #ect_class_plan_values
    #ect_bk_class_plan_values


  end


  def show
    #index
    #class_plan_values
    @engagement_id = params[:engagement_id].to_s
    @plan_version_id = params[:plan_version_id].to_s

    #過去バージョンのデータを取得する
    @engagements = EctBkEngagements.all
    @engagements = @engagements.where(:engagement_id => @engagement_id)
    @engagements = @engagements.where(:plan_version_id => @plan_version_id)

    #前のshowを使う
    render :template => "ect/engagements/show"

  end

end
