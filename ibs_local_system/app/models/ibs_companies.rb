class IbsCompanies < ActiveRecord::Base

  #
  has_many :ibs_local_job_levels
  has_many :ibs_activity_types
  has_many :ibs_engagements

end
