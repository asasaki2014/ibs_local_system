require 'test_helper'

class EngagementsControllerTest < ActionController::TestCase
  setup do
    @engagement = engagements(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:engagements)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create engagement" do
    assert_difference('Engagement.count') do
      post :create, engagement: { assistant_legacy_emplid: @engagement.assistant_legacy_emplid, assistant_name: @engagement.assistant_name, assistant_sap_personnel_number: @engagement.assistant_sap_personnel_number, company_code: @engagement.company_code, confidential_engagment: @engagement.confidential_engagment, contract_amount: @engagement.contract_amount, contract_type: @engagement.contract_type, created_at: @engagement.created_at, deleted_at: @engagement.deleted_at, em_legacy_emplid: @engagement.em_legacy_emplid, em_name: @engagement.em_name, em_sap_personnel_number: @engagement.em_sap_personnel_number, engagement_descr: @engagement.engagement_descr, engagement_legacy_number: @engagement.engagement_legacy_number, engagement_number: @engagement.engagement_number, engagement_status: @engagement.engagement_status, engagement_type: @engagement.engagement_type, engagement_type_descr: @engagement.engagement_type_descr, entity_name: @engagement.entity_name, entity_number: @engagement.entity_number, ep_legacy_emplid: @engagement.ep_legacy_emplid, ep_name: @engagement.ep_name, ep_sap_personnel_number: @engagement.ep_sap_personnel_number, erp_realization_rate: @engagement.erp_realization_rate, fiscal_end_date: @engagement.fiscal_end_date, lead_engagement_number: @engagement.lead_engagement_number, local_service_type: @engagement.local_service_type, local_service_type_descr: @engagement.local_service_type_descr, profit_center: @engagement.profit_center, profit_center_descr: @engagement.profit_center_descr, san_expirty_date: @engagement.san_expirty_date, san_start_date: @engagement.san_start_date, updated_at: @engagement.updated_at }
    end

    assert_redirected_to engagement_path(assigns(:engagement))
  end

  test "should show engagement" do
    get :show, id: @engagement
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @engagement
    assert_response :success
  end

  test "should update engagement" do
    patch :update, id: @engagement, engagement: { assistant_legacy_emplid: @engagement.assistant_legacy_emplid, assistant_name: @engagement.assistant_name, assistant_sap_personnel_number: @engagement.assistant_sap_personnel_number, company_code: @engagement.company_code, confidential_engagment: @engagement.confidential_engagment, contract_amount: @engagement.contract_amount, contract_type: @engagement.contract_type, created_at: @engagement.created_at, deleted_at: @engagement.deleted_at, em_legacy_emplid: @engagement.em_legacy_emplid, em_name: @engagement.em_name, em_sap_personnel_number: @engagement.em_sap_personnel_number, engagement_descr: @engagement.engagement_descr, engagement_legacy_number: @engagement.engagement_legacy_number, engagement_number: @engagement.engagement_number, engagement_status: @engagement.engagement_status, engagement_type: @engagement.engagement_type, engagement_type_descr: @engagement.engagement_type_descr, entity_name: @engagement.entity_name, entity_number: @engagement.entity_number, ep_legacy_emplid: @engagement.ep_legacy_emplid, ep_name: @engagement.ep_name, ep_sap_personnel_number: @engagement.ep_sap_personnel_number, erp_realization_rate: @engagement.erp_realization_rate, fiscal_end_date: @engagement.fiscal_end_date, lead_engagement_number: @engagement.lead_engagement_number, local_service_type: @engagement.local_service_type, local_service_type_descr: @engagement.local_service_type_descr, profit_center: @engagement.profit_center, profit_center_descr: @engagement.profit_center_descr, san_expirty_date: @engagement.san_expirty_date, san_start_date: @engagement.san_start_date, updated_at: @engagement.updated_at }
    assert_redirected_to engagement_path(assigns(:engagement))
  end

  test "should destroy engagement" do
    assert_difference('Engagement.count', -1) do
      delete :destroy, id: @engagement
    end

    assert_redirected_to engagements_path
  end
end
