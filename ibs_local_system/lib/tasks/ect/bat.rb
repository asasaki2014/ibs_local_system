class Tasks::Ect::Bat < ActiveRecord::Base

  #batでの流し込み処理
  def self.execute
   for i in 0..50

      #sample
      @new_ibs_engagement = IbsEngagements.new(
        :ibs_company_id => i  , # 会社ID  t.integer
        :ibs_profit_center_id => i  , # 利益センタID t.integer
        :engagement_number => i  , # エンゲージメント番号  t.string
        :engagement_descr => "" , # エンゲージメント名称  t.string
        :engagement_legacy_number => "エンゲージメントレガシー番号" , # エンゲージメントレガシー番号  t.string
        :engagement_closed_date => "2014-05-01" , # t.date
        :currency => 10.2 , # t.decimal
        :business_area => "aaaa"  , # t.string
        :engagement_status_effective_date => "2014-10-01" , # t.date
        :em_sap_personnel_number => i , # EM SAP職員番号  t.integer
        :em_legacy_emplid => "a"  , # EM Legacy 従業員ID t.string
        :ep_sap_personnel_number => 1 , # EP SAP職員番号  t.integer
        :ep_legacy_emplid => "b"  , # EP Legacy 従業員ID t.string
        :erp_realization_rate => 10.2 , # ERP実現値  t.decimal
        :sector_code => "bbbb"  , # t.string
        :line_of_business => "aa" , # t.string
        :estimated_recoverable_fees => "abcdefg"  , # t.string
        :entity_number => "cccccccccc"  , # t.string
        :engagement_open_date => "2014-10-03" , # t.date
        :profit_center => "利益センタ" , # 利益センタ t.string
        :local_service_type => "ローカルサービスタイプ"  , # ローカルサービスタイプ t.string
        :local_service_type_descr => "bbbb" , # ローカルサービスタイプ名称 t.string
        :company_code => "sdga" , # 会社コード t.string
        :sic_code => "aaaa" , # t.string
        :engagement_status => "sadf"  , # エンゲージメントステータス t.string
        :engagement_type => "wwww"  , # エンゲージメントタイプ t.string
        :recurring_work_descr => "aaaaa"  , # t.string
        :profit_center_descr => "adfaedf" , # 利益センタ名称 t.string
        :ceac_era_id => "adsaga"  , # t.string
        :engagement_create_date => "2014-05-01" , # t.date
        :assistant_name => "adsafefe" , # EA氏名  t.string
        :assistant_sap_personnel_number => 1234 , # EA職員番号  t.integer
        :assistant_legacy_emplid => ""  , # EA Legacy 従業員ID t.string
        :san_number => "" , # t.string
        :em_name => "aaa" , # EM氏名  t.string
        :ep_name => "adfaef"  , # EP氏名  t.string
        :engagement_type_descr => ""  , # エンゲージメントタイプ名称 t.string
        :entity_name => ""  , # エンティティ名称  t.string
        :ep_email => "" , # t.string
        :function => "" , # t.string
        :engagement_update_date => "2014-05-10" , # t.date
        :associated_partner_name => 40  , # t.string
        :associated_partner_sap_personnel_number => ""  , # t.integer
        :associated_partner_legacy_emplid => "" , # t.string
        :engagement_accreditation1 => ""  , # t.string
        :engagement_accreditation2 => ""  , # t.string
        :engagement_accreditation3 => ""  , # t.string
        :engagement_accreditation4 => ""  , # t.string
        :engagement_accreditation5 => ""  , # t.string
        :lead_engagement_number => "" , # LEADエンゲージメント番号  t.string
        :origin => "" , # t.string
        :recurring__engagement__date => "2015-01-01"  , # t.date
        :fiscal_year => 1 , # t.integer
        :san_expirty_date => "2015-02-01" , # 終了日付  t.date
        :sentinel_service_type => ""  , # t.string
        :fa_code => ""  , # t.string
        :client_risk__rating => ""  , # t.string
        :cead_id => ""  , # t.string
        :engagment_risk_rating => ""  , # t.string
        :request_sent_on => "2013-06-01"  , # t.date
        :approved_on => "2014-10-01"  , # t.date
        :designated_approver => ""  , # t.string
        :designated_approver_name => "" , # t.integer
        :zero_percentage_realization => "a" , # t.string
        :confidential_engagment => "" , # 秘匿エンゲージメントフラグ t.string
        :honorary_engagment => "" , # t.string
        :rate_card_engagment => ""  , # t.string
        :user_special_price => "" , # t.string
        :retroactive => ""  , # t.string
        :billing_office => "" , # t.string
        :remittance_office => ""  , # t.string
        :reference_ext => ""  , # t.string
        :reference_kpmg_emp => "A"  , # t.integer
        :opportunity_number => "" , # t.string
        :tax_classfication => "a" , # t.string
        :terms_of_payment => "a" , # t.string
        :san_start_date => "2016-01-01" , # 開始日付  t.date
        :contract_amount => 10000.20  , # 契約金額  t.decimal
        :fiscal_end_date => "2014-06-01"  , # 決算日 t.date
        :contract_type => ""  , # t.string

      )

      #チェック
      if( @new_ibs_engagement.save )
        logger.debug "save_ok" + i.to_s
        puts "save_ok" + i.to_s
      else
        logger.debug "save_error" + i.to_s
        puts "save_error" + i.to_s
      end

    end


=begin
t.integer  :ibs_company_id    # 会社ID
t.integer  :ibs_profit_center_id    # 利益センタID
t.string   :engagement_number , :limit=>10  # エンゲージメント番号
t.string   :engagement_descr  , :limit=>40  # エンゲージメント名称
t.string   :engagement_legacy_number  , :limit=>35  # エンゲージメントレガシー番号
t.date   :engagement_closed_date    #
t.decimal  :currency  , :precision=>10, :scale=>2    #
t.string   :business_area , :limit=>4 #
t.date   :engagement_status_effective_date    #
t.integer  :em_sap_personnel_number   # EM SAP職員番号
t.string   :em_legacy_emplid  , :limit=>12  # EM Legacy 従業員ID
t.integer  :ep_sap_personnel_number   # EP SAP職員番号
t.string   :ep_legacy_emplid  , :limit=>12  # EP Legacy 従業員ID
t.decimal  :erp_realization_rate  , :precision=>10, :scale=>2 # ERP実現値
t.string   :sector_code , :limit=>4 #
t.string   :line_of_business  , :limit=>2 #
t.string   :estimated_recoverable_fees  , :limit=>20  #
t.string   :entity_number , :limit=>10  # エンティティ番号
t.date   :engagement_open_date    #
t.string   :profit_center , :limit=>10  # 利益センタ
t.string   :local_service_type  , :limit=>18  # ローカルサービスタイプ
t.string   :local_service_type_descr  , :limit=>40  # ローカルサービスタイプ名称
t.string   :company_code  , :limit=>4 # 会社コード
t.string   :sic_code  , :limit=>10  #
t.string   :engagement_status , :limit=>30  # エンゲージメントステータス
t.string   :engagement_type , :limit=>4 # エンゲージメントタイプ
t.string   :recurring_work_descr  , :limit=>50  #
t.string   :profit_center_descr , :limit=>40  # 利益センタ名称
t.string   :ceac_era_id , :limit=>24  #
t.date   :engagement_create_date    #
t.string   :assistant_name  , :limit=>40  # EA氏名
t.integer  :assistant_sap_personnel_number    # EA職員番号
t.string   :assistant_legacy_emplid , :limit=>12  # EA Legacy 従業員ID
t.string   :san_number  , :limit=>10  #
t.string   :em_name , :limit=>40  # EM氏名
t.string   :ep_name , :limit=>40  # EP氏名
t.string   :engagement_type_descr , :limit=>20  # エンゲージメントタイプ名称
t.string   :entity_name , :limit=>35  # エンティティ名称
t.string   :ep_email  , :limit=>241 #
t.string   :function  , :limit=>20  #
t.date   :engagement_update_date    #
t.string   :associated_partner_name , :limit=>40  #
t.integer  :associated_partner_sap_personnel_number   #
t.string   :associated_partner_legacy_emplid  , :limit=>12  #
t.string   :engagement_accreditation1 , :limit=>1 #
t.string   :engagement_accreditation2 , :limit=>1 #
t.string   :engagement_accreditation3 , :limit=>1 #
t.string   :engagement_accreditation4 , :limit=>1 #
t.string   :engagement_accreditation5 , :limit=>1 #
t.string   :lead_engagement_number  , :limit=>10  # LEADエンゲージメント番号
t.string   :origin  , :limit=>4 #
t.date   :recurring__engagement__date   #
t.integer  :fiscal_year   #
t.date   :san_expirty_date    # 終了日付
t.string   :sentinel_service_type , :limit=>18  #
t.string   :fa_code , :limit=>10  #
t.string   :client_risk__rating , :limit=>2 #
t.string   :cead_id , :limit=>10  #
t.string   :engagment_risk_rating , :limit=>2 #
t.date   :request_sent_on   #
t.date   :approved_on   #
t.string   :designated_approver , :limit=>12  #
t.integer  :designated_approver_name    #
t.string   :zero_percentage_realization , :limit=>1 #
t.string   :confidential_engagment  , :limit=>1 # 秘匿エンゲージメントフラグ
t.string   :honorary_engagment  , :limit=>1 #
t.string   :rate_card_engagment , :limit=>1 #
t.string   :user_special_price  , :limit=>1 #
t.string   :retroactive , :limit=>1 #
t.string   :billing_office  , :limit=>42  #
t.string   :remittance_office , :limit=>42  #
t.string   :reference_ext , :limit=>40  #
t.integer  :reference_kpmg_emp    #
t.string   :opportunity_number  , :limit=>10  #
t.string   :tax_classfication , :limit=>1 #
t.string   :terms_of_payment  , :limit=>4 #
t.date   :san_start_date    # 開始日付
t.decimal  :contract_amount , :precision=>15, :scale=>2 # 契約金額
t.date   :fiscal_end_date    # 決算日
t.string   :contract_type , :limit=>2 # 契約タイプ
t.datetime   :created_at    # レコード作成日時
t.datetime   :updated_at    # レコード更新日時
t.datetime   :deleted_at    # レコード削除日時
=end

  end
end