class CreateEctBkClassPlanValues < ActiveRecord::Migration
  def change
    create_table :ect_bk_class_plan_values do |t|

t.string   :job_level_code  , :limit=>4 # ジョブレベルコード

t.decimal  :plan_hour , :precision=>10, :scale=>2 # 予定時間
t.decimal  :plan_standard_charge_out_amount , :precision=>15, :scale=>2 # 予定標準チャージアウト金額
t.decimal  :actual_hour_summary , :precision=>10, :scale=>2 # 実績時間合計
t.decimal  :actual_amount_summary , :precision=>15, :scale=>2 # 実績標準チャージアウト金額
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

#      t.timestamps
    end
  end
end
