class CreateIbsCompanies < ActiveRecord::Migration
  def change
    create_table :ibs_companies do |t|

t.string   :company_code  , :limit=>4 # 会社コード
t.string   :legacy_company_code , :limit=>4 # レガシー会社コード
t.string   :company_name  , :limit=>25  # 会社名称
t.string   :city  , :limit=>25  #
t.string   :country , :limit=>3 #
t.string   :language  , :limit=>4 #
t.string   :currency  , :limit=>5 #
t.string   :name_print  , :limit=>160 #
t.string   :name_local_language , :limit=>25  # 会社名称（ローカル言語）
t.string   :street  , :limit=>60  #
t.string   :street_2  , :limit=>60  #
t.string   :street_3  , :limit=>60  #
t.string   :street_local_language , :limit=>60  #
t.string   :street_2_local_language , :limit=>60  #
t.string   :street_3_local_language , :limit=>60  #
t.string   :post_code , :limit=>10  #
t.string   :telephone , :limit=>30  #
t.string   :fax , :limit=>30  #
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

#      t.timestamps
    end
  end
end
