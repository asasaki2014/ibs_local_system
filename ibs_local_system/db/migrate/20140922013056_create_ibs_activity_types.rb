class CreateIbsActivityTypes < ActiveRecord::Migration
  def change
    create_table :ibs_activity_types do |t|

t.integer  :ibs_company_id    # 会社ID
t.string   :company_code  , :limit=>4 # 会社コード
t.string   :company_name  , :limit=>25  # 会社名称
t.text   :function    #
t.string   :staff_level , :limit=>4 # スタッフレベル
t.string   :region_personnel_area , :limit=>4 #
t.string   :location_personnel_subarea  , :limit=>4 #
t.string   :profit_center , :limit=>8 # 利益センタ
t.string   :activity_type , :limit=>6 #
t.string   :activity_type_name  , :limit=>20  #
t.string   :activity_type_description , :limit=>40  #
t.decimal  :charge_rate , :precision=>15, :scale=>2 # チャージレート
t.decimal  :cost_rate , :precision=>15, :scale=>2 # コストレート
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時


#      t.timestamps
    end
  end

  #20140922013044_create_ibs_companies.rb
  #の子供



end
