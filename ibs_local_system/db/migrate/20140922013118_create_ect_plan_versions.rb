class CreateEctPlanVersions < ActiveRecord::Migration
  def change
    create_table :ect_plan_versions do |t|

t.integer  :plan_version_number   # 計画バージョン番号
t.datetime   :plan_last_update_date   # 計画最終更新日時
t.text   :plan_last_update_user_name    # 計画最終更新者
t.string   :engagement_number , :limit=>10  # エンゲージメント番号
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

#      t.timestamps
    end
  end
end
