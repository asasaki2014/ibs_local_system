class CreateEctBkEngagements < ActiveRecord::Migration
  def change
    create_table :ect_bk_engagements do |t|

t.string   :engagement_number , :limit=>10  # エンゲージメント番号
t.string   :engagement_descr  , :limit=>40  # エンゲージメント名称
t.string   :engagement_legacy_number  , :limit=>35  # エンゲージメントレガシー番号
t.integer  :em_sap_personnel_number   # EM SAP職員番号
t.string   :em_legacy_emplid  , :limit=>12  # EM Legacy 従業員ID
t.integer  :ep_sap_personnel_number   # EP SAP職員番号
t.string   :ep_legacy_emplid  , :limit=>12  # EP Legacy 従業員ID
t.decimal  :erp_realization_rate  , :precision=>11, :scale=>2 # ERP実現値
t.string   :entity_number , :limit=>10  # エンティティ番号
t.string   :profit_center , :limit=>10  # 利益センタ
t.string   :local_service_type  , :limit=>18  # ローカルサービスタイプ
t.string   :local_service_type_descr  , :limit=>40  # ローカルサービスタイプ名称
t.string   :company_code  , :limit=>4 # 会社コード
t.string   :engagement_status , :limit=>30  # エンゲージメントステータス
t.string   :engagement_type , :limit=>4 # エンゲージメントタイプ
t.string   :profit_center_descr , :limit=>40  # 利益センタ名称
t.string   :assistant_name  , :limit=>40  # EA氏名
t.integer  :assistant_sap_personnel_number    # EA職員番号
t.string   :assistant_legacy_emplid , :limit=>12  # EA Legacy 従業員ID
t.string   :em_name , :limit=>40  # EM氏名
t.string   :ep_name , :limit=>40  # EP氏名
t.string   :engagement_type_descr , :limit=>20  # エンゲージメントタイプ名称
t.string   :entity_name , :limit=>35  # エンティティ名称
t.string   :lead_engagement_number  , :limit=>10  # LEADエンゲージメント番号
t.date   :san_expirty_date    # 終了日付
t.string   :confidential_engagment  , :limit=>1 # 秘匿エンゲージメントフラグ
t.date   :san_start_date    # 開始日付
t.decimal  :contract_amount , :precision=>15, :scale=>2 # 契約金額
t.date   :fiscal_end_date  # 決算日
t.string   :contract_type , :limit=>2 # 契約タイプ
t.integer  :plan_version_number   # 計画バージョン番号
t.decimal  :planned_subcontract_fee , :precision=>15, :scale=>2 # 計画外注費
t.decimal  :actual_subcontract_fee  , :precision=>15, :scale=>2 # 実績外注費
t.decimal  :actual_hour , :precision=>10, :scale=>2 # 実績時間
t.decimal  :actual_revenue  , :precision=>15, :scale=>2 # 実績金額
t.decimal  :total_contract_amount , :precision=>15, :scale=>2 # 契約金額Lead+Sub合計
t.datetime   :created_at    # レコード作成日時
t.datetime   :updated_at    # レコード更新日時
t.datetime   :deleted_at    # レコード削除日時

#      t.timestamps
    end
  end
end
