class CreateIbsProfitCenters < ActiveRecord::Migration
  def change
    create_table :ibs_profit_centers do |t|

t.text   :member_firm   #
t.text   :client_service_orclient_service_support    #
t.text   :function    #
t.text   :service_group   #
t.text   :mf_defined_pc_group   #
t.text   :mf_defined_pc_group_name    #
t.text   :check_length_of_pc_grp_name   #
t.string   :profit_center , :limit=>10  # 利益センタ
t.string   :profit_center_name_long , :limit=>40  # 利益センタ名称Long
t.string   :profit_center_name_short  , :limit=>20  # 利益センタ名称Short
t.text   :check_length_of_pc_name   #
t.string   :pc_name_long  , :limit=>40  # 利益センタ名称ローカルLong
t.string   :pc_name_short , :limit=>20  # 利益センタ名称ローカルShort
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

      t.timestamps
    end
  end



end
