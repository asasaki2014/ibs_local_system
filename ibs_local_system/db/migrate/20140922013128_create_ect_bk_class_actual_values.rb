class CreateEctBkClassActualValues < ActiveRecord::Migration
  def change
    create_table :ect_bk_class_actual_values do |t|

t.integer  :ect_bk_class_plan_value_id    # 過去分クラス別計画値ID
t.date   :actual_year_month   # 実績年月
t.decimal  :actual_hour , :precision=>10, :scale=>2 # 実績時間
t.decimal  :actual_amount , :precision=>15, :scale=>2 # 実績金額
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

#      t.timestamps
    end
  end

  #20140922013125_create_ect_bk_class_plan_values.rb
  #の子供


end
