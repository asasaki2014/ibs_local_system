class CreateEctClassPlanValues < ActiveRecord::Migration
  def change
    create_table :ect_class_plan_values do |t|

t.integer  :ibs_engagement_id   # エンゲージメントID
t.integer  :ibs_local_job_level_id    # ローカルジョブレベルID
t.string   :job_level_code  , :limit=>4 # ジョブレベルコード
t.decimal  :plan_hour , :precision=>10, :scale=>2 # 予定時間
t.decimal  :actual_hour_summary , :precision=>10, :scale=>2 # 実績時間合計
t.decimal  :actual_amount_summary , :precision=>15, :scale=>2 # 実績標準チャージアウト金額
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

#      t.timestamps
    end
  end

  #20140922013100_create_ibs_engagements.rb
  #の子供


end
