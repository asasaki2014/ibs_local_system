class CreateIbsLocalJobLevels < ActiveRecord::Migration
  def change
    create_table :ibs_local_job_levels do |t|

t.integer  :ibs_company_id    # 会社ID
t.string   :job_level_code  , :limit=>4 # ジョブレベルコード
t.text   :job_level_text    # ジョブレベルテキスト
t.integer  :category    #
t.integer  :rest    #
t.text   :global_job_levels   #
t.string   :company_code  , :limit=>4 # 会社コード
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

#      t.timestamps
    end
  end

  #20140922013044_create_ibs_companies.rb
  #の子供

end
