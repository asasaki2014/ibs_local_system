class CreateIbsTeamMembers < ActiveRecord::Migration
  def change
    create_table :ibs_team_members do |t|

t.integer  :ibs_engagement_id   # エンゲージメントID
t.string   :engagement_number , :limit=>10  # エンゲージメント番号
t.integer  :partner   #
t.string   :partner_function  , :limit=>2 #
t.date   :start_date  #
t.string   :employee_name , :limit=>40  # 従業員名
t.string   :user_id , :limit=>12  # ユーザーID
t.string   :access  , :limit=>11  #
t.date   :end_date   #
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

#      t.timestamps
    end
  end

  #20140922013100_create_ibs_engagements.rb
  #の子供

end
