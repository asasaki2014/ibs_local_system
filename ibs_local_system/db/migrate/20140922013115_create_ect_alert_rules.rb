class CreateEctAlertRules < ActiveRecord::Migration
  def change
    create_table :ect_alert_rules do |t|

t.integer  :priority    # 優先順位
t.integer  :relative_fiscal_end_month   # 決算年月までの月数
t.decimal  :progress_rate , :precision=>10, :scale=>2 # 進捗率
t.string   :bg_color  , :limit=>7 # 背景色
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

#      t.timestamps
    end
  end
end
