# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140922013128) do

  create_table "ect_alert_rules", force: true do |t|
    t.integer  "priority"
    t.integer  "relative_fiscal_end_month"
    t.decimal  "progress_rate",                       precision: 10, scale: 2
    t.string   "bg_color",                  limit: 7
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  create_table "ect_bk_class_actual_values", force: true do |t|
    t.integer  "ect_bk_class_plan_value_id"
    t.date     "actual_year_month"
    t.decimal  "actual_hour",                precision: 10, scale: 2
    t.decimal  "actual_amount",              precision: 15, scale: 2
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  create_table "ect_bk_class_plan_values", force: true do |t|
    t.string   "job_level_code",                  limit: 4
    t.decimal  "plan_hour",                                 precision: 10, scale: 2
    t.decimal  "plan_standard_charge_out_amount",           precision: 15, scale: 2
    t.decimal  "actual_hour_summary",                       precision: 10, scale: 2
    t.decimal  "actual_amount_summary",                     precision: 15, scale: 2
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  create_table "ect_bk_engagements", force: true do |t|
    t.string   "engagement_number",              limit: 10
    t.string   "engagement_descr",               limit: 40
    t.string   "engagement_legacy_number",       limit: 35
    t.integer  "em_sap_personnel_number"
    t.string   "em_legacy_emplid",               limit: 12
    t.integer  "ep_sap_personnel_number"
    t.string   "ep_legacy_emplid",               limit: 12
    t.decimal  "erp_realization_rate",                      precision: 11, scale: 2
    t.string   "entity_number",                  limit: 10
    t.string   "profit_center",                  limit: 10
    t.string   "local_service_type",             limit: 18
    t.string   "local_service_type_descr",       limit: 40
    t.string   "company_code",                   limit: 4
    t.string   "engagement_status",              limit: 30
    t.string   "engagement_type",                limit: 4
    t.string   "profit_center_descr",            limit: 40
    t.string   "assistant_name",                 limit: 40
    t.integer  "assistant_sap_personnel_number"
    t.string   "assistant_legacy_emplid",        limit: 12
    t.string   "em_name",                        limit: 40
    t.string   "ep_name",                        limit: 40
    t.string   "engagement_type_descr",          limit: 20
    t.string   "entity_name",                    limit: 35
    t.string   "lead_engagement_number",         limit: 10
    t.date     "san_expirty_date"
    t.string   "confidential_engagment",         limit: 1
    t.date     "san_start_date"
    t.decimal  "contract_amount",                           precision: 15, scale: 2
    t.date     "fiscal_end_date"
    t.string   "contract_type",                  limit: 2
    t.integer  "plan_version_number"
    t.decimal  "planned_subcontract_fee",                   precision: 15, scale: 2
    t.decimal  "actual_subcontract_fee",                    precision: 15, scale: 2
    t.decimal  "actual_hour",                               precision: 10, scale: 2
    t.decimal  "actual_revenue",                            precision: 15, scale: 2
    t.decimal  "total_contract_amount",                     precision: 15, scale: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "ect_class_actual_values", force: true do |t|
    t.integer  "ect_class_plan_value_id"
    t.date     "actual_year_month"
    t.decimal  "actual_hour",             precision: 10, scale: 2
    t.decimal  "actual_amount",           precision: 15, scale: 2
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  create_table "ect_class_plan_values", force: true do |t|
    t.integer  "ibs_engagement_id"
    t.integer  "ibs_local_job_level_id"
    t.string   "job_level_code",         limit: 4
    t.decimal  "plan_hour",                        precision: 10, scale: 2
    t.decimal  "actual_hour_summary",              precision: 10, scale: 2
    t.decimal  "actual_amount_summary",            precision: 15, scale: 2
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  create_table "ect_plan_versions", force: true do |t|
    t.integer  "plan_version_number"
    t.datetime "plan_last_update_date"
    t.text     "plan_last_update_user_name"
    t.string   "engagement_number",          limit: 10
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  create_table "ibs_activity_types", force: true do |t|
    t.integer  "ibs_company_id"
    t.string   "company_code",               limit: 4
    t.string   "company_name",               limit: 25
    t.text     "function"
    t.string   "staff_level",                limit: 4
    t.string   "region_personnel_area",      limit: 4
    t.string   "location_personnel_subarea", limit: 4
    t.string   "profit_center",              limit: 8
    t.string   "activity_type",              limit: 6
    t.string   "activity_type_name",         limit: 20
    t.string   "activity_type_description",  limit: 40
    t.decimal  "charge_rate",                           precision: 15, scale: 2
    t.decimal  "cost_rate",                             precision: 15, scale: 2
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  create_table "ibs_companies", force: true do |t|
    t.string   "company_code",            limit: 4
    t.string   "legacy_company_code",     limit: 4
    t.string   "company_name",            limit: 25
    t.string   "city",                    limit: 25
    t.string   "country",                 limit: 3
    t.string   "language",                limit: 4
    t.string   "currency",                limit: 5
    t.string   "name_print",              limit: 160
    t.string   "name_local_language",     limit: 25
    t.string   "street",                  limit: 60
    t.string   "street_2",                limit: 60
    t.string   "street_3",                limit: 60
    t.string   "street_local_language",   limit: 60
    t.string   "street_2_local_language", limit: 60
    t.string   "street_3_local_language", limit: 60
    t.string   "post_code",               limit: 10
    t.string   "telephone",               limit: 30
    t.string   "fax",                     limit: 30
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  create_table "ibs_engagements", force: true do |t|
    t.integer  "ibs_company_id"
    t.integer  "ibs_profit_center_id"
    t.string   "engagement_number",                       limit: 10
    t.string   "engagement_descr",                        limit: 40
    t.string   "engagement_legacy_number",                limit: 35
    t.date     "engagement_closed_date"
    t.decimal  "currency",                                            precision: 10, scale: 2
    t.string   "business_area",                           limit: 4
    t.date     "engagement_status_effective_date"
    t.integer  "em_sap_personnel_number"
    t.string   "em_legacy_emplid",                        limit: 12
    t.integer  "ep_sap_personnel_number"
    t.string   "ep_legacy_emplid",                        limit: 12
    t.decimal  "erp_realization_rate",                                precision: 10, scale: 2
    t.string   "sector_code",                             limit: 4
    t.string   "line_of_business",                        limit: 2
    t.string   "estimated_recoverable_fees",              limit: 20
    t.string   "entity_number",                           limit: 10
    t.date     "engagement_open_date"
    t.string   "profit_center",                           limit: 10
    t.string   "local_service_type",                      limit: 18
    t.string   "local_service_type_descr",                limit: 40
    t.string   "company_code",                            limit: 4
    t.string   "sic_code",                                limit: 10
    t.string   "engagement_status",                       limit: 30
    t.string   "engagement_type",                         limit: 4
    t.string   "recurring_work_descr",                    limit: 50
    t.string   "profit_center_descr",                     limit: 40
    t.string   "ceac_era_id",                             limit: 24
    t.date     "engagement_create_date"
    t.string   "assistant_name",                          limit: 40
    t.integer  "assistant_sap_personnel_number"
    t.string   "assistant_legacy_emplid",                 limit: 12
    t.string   "san_number",                              limit: 10
    t.string   "em_name",                                 limit: 40
    t.string   "ep_name",                                 limit: 40
    t.string   "engagement_type_descr",                   limit: 20
    t.string   "entity_name",                             limit: 35
    t.string   "ep_email",                                limit: 241
    t.string   "function",                                limit: 20
    t.date     "engagement_update_date"
    t.string   "associated_partner_name",                 limit: 40
    t.integer  "associated_partner_sap_personnel_number"
    t.string   "associated_partner_legacy_emplid",        limit: 12
    t.string   "engagement_accreditation1",               limit: 1
    t.string   "engagement_accreditation2",               limit: 1
    t.string   "engagement_accreditation3",               limit: 1
    t.string   "engagement_accreditation4",               limit: 1
    t.string   "engagement_accreditation5",               limit: 1
    t.string   "lead_engagement_number",                  limit: 10
    t.string   "origin",                                  limit: 4
    t.date     "recurring__engagement__date"
    t.integer  "fiscal_year"
    t.date     "san_expirty_date"
    t.string   "sentinel_service_type",                   limit: 18
    t.string   "fa_code",                                 limit: 10
    t.string   "client_risk__rating",                     limit: 2
    t.string   "cead_id",                                 limit: 10
    t.string   "engagment_risk_rating",                   limit: 2
    t.date     "request_sent_on"
    t.date     "approved_on"
    t.string   "designated_approver",                     limit: 12
    t.integer  "designated_approver_name"
    t.string   "zero_percentage_realization",             limit: 1
    t.string   "confidential_engagment",                  limit: 1
    t.string   "honorary_engagment",                      limit: 1
    t.string   "rate_card_engagment",                     limit: 1
    t.string   "user_special_price",                      limit: 1
    t.string   "retroactive",                             limit: 1
    t.string   "billing_office",                          limit: 42
    t.string   "remittance_office",                       limit: 42
    t.string   "reference_ext",                           limit: 40
    t.integer  "reference_kpmg_emp"
    t.string   "opportunity_number",                      limit: 10
    t.string   "tax_classfication",                       limit: 1
    t.string   "terms_of_payment",                        limit: 4
    t.date     "san_start_date"
    t.decimal  "contract_amount",                                     precision: 15, scale: 2
    t.date     "fiscal_end_date"
    t.string   "contract_type",                           limit: 2
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
  end

  create_table "ibs_local_job_levels", force: true do |t|
    t.integer  "ibs_company_id"
    t.string   "job_level_code",    limit: 4
    t.text     "job_level_text"
    t.integer  "category"
    t.integer  "rest"
    t.text     "global_job_levels"
    t.string   "company_code",      limit: 4
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  create_table "ibs_profit_centers", force: true do |t|
    t.text     "member_firm"
    t.text     "client_service_orclient_service_support"
    t.text     "function"
    t.text     "service_group"
    t.text     "mf_defined_pc_group"
    t.text     "mf_defined_pc_group_name"
    t.text     "check_length_of_pc_grp_name"
    t.string   "profit_center",                           limit: 10
    t.string   "profit_center_name_long",                 limit: 40
    t.string   "profit_center_name_short",                limit: 20
    t.text     "check_length_of_pc_name"
    t.string   "pc_name_long",                            limit: 40
    t.string   "pc_name_short",                           limit: 20
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

  create_table "ibs_team_members", force: true do |t|
    t.integer  "ibs_engagement_id"
    t.string   "engagement_number", limit: 10
    t.integer  "partner"
    t.string   "partner_function",  limit: 2
    t.date     "start_date"
    t.string   "employee_name",     limit: 40
    t.string   "user_id",           limit: 12
    t.string   "access",            limit: 11
    t.date     "end_date"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
  end

end
