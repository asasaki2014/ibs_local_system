class CreateAlertRules < ActiveRecord::Migration

  # テーブル名   alert_rules
  # エンティティ名称    アラートルール
  # 説明    相対的な決算年月と進捗率毎のEngagement一覧の背景色を保持する

  def change
    create_table :alert_rules do |t|
t.integer  :priority    # 優先順位
t.integer  :relative_fiscal_end_month   # 決算年月までの月数
t.decimal  :progress_rate , :precision=>10, :scale=>2 # 進捗率
t.string   :bg_color  , :limit=>7 # 背景色
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時


      t.integer :lock_version, :default => 0
#     t.timestamps
    end
  end
end
