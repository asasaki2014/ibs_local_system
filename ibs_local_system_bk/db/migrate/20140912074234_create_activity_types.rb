class CreateActivityTypes < ActiveRecord::Migration

  # テーブル名   activity_types
  # エンティティ名称    アクティビティタイプ（レート）
  # 説明    activity_typesを源泉元とするテーブル

  def change
    create_table :activity_types do |t|
t.string   :company_code  , :limit=>4 # 会社コード
t.text   :company_name    # 会社名称
t.string   :staff_level , :limit=>4 # スタッフレベル
t.string   :profit_center , :limit=>8 # 利益センタ
t.decimal  :charge_rate , :precision=>17, :scale=>0 # チャージレート
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時


      t.integer :lock_version, :default => 0
#     t.timestamps
    end
  end
end
