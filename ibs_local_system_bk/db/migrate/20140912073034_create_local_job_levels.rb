class CreateLocalJobLevels < ActiveRecord::Migration

  # テーブル名   local_job_levels
  # エンティティ名称    ローカルジョブレベル（クラス）
  # 説明    LocalJobLevelsを源泉元とするテーブル

  def change
    create_table :local_job_levels do |t|
t.string   :job_level_code  , :limit=>4 # ジョブレベルコード
t.text   :job_level_text    # ジョブレベルテキスト
t.string   :company_code  , :limit=>4 # 会社コード
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時\

      t.integer :lock_version, :default => 0
#     t.timestamps
    end
  end
end
