class CreateCompanies < ActiveRecord::Migration

  # テーブル名   companies
  # エンティティ名称    会社
  # 説明    legal_entitiesを源泉元とするテーブル

  def change
    create_table :companies do |t|
t.string   :company_code  , :limit=>4 # 会社コード
t.string   :legacy_company_code , :limit=>4 # レガシー会社コード
t.string   :company_name  , :limit=>25  # 会社名称
t.text   :name_local_language   # 会社名称（ローカル言語）
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

      t.integer :lock_version, :default => 0
#     t.timestamps
    end
  end
end
