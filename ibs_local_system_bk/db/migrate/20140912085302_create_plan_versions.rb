class CreatePlanVersions < ActiveRecord::Migration

  # テーブル名   plan_versions
  # エンティティ名称    計画バージョン
  # 説明    計画バージョン

  def change
    create_table :plan_versions do |t|
t.datetime   :plan_last_update_date   # 計画最終更新日時
t.text   :plan_last_update_user_name    # 計画最終更新者
t.string   :engagement_number , :limit=>10  # エンゲージメント番号
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

      t.integer :lock_version, :default => 0
#     t.timestamps
    end
  end
end
