class CreateClassActualValues < ActiveRecord::Migration

  # テーブル名   class_actual_values
  # エンティティ名称    クラス別実績値
  # 説明    IBSから送信されてくる実績データを、バッチでクラス/年月毎に集計したテーブル

  def change
    create_table :class_actual_values do |t|
t.string   :job_level_code  , :limit=>4 # ジョブレベルコード
t.integer  :plan_version_id   # 計画バージョンID
t.date   :actual_year_month   # 実績年月
t.decimal  :actual_hour , :precision=>10, :scale=>2 # 実績時間
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時

      t.integer :lock_version, :default => 0
#     t.timestamps
    end
  end
end
