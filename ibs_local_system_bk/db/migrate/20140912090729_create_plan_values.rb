class CreatePlanValues < ActiveRecord::Migration

  # テーブル名   plan_values
  # エンティティ名称    クラス別計画値
  # 説明    クラス別計画値を保持する

  def change
    create_table :plan_values do |t|
t.string   :job_level_code  , :limit=>4 # ジョブレベルコード
t.integer  :plan_version_id   # 計画バージョンID
t.decimal  :plan_hour , :precision=>10, :scale=>2 # 予定時間
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時


      t.integer :lock_version, :default => 0
#     t.timestamps
    end
  end
end
