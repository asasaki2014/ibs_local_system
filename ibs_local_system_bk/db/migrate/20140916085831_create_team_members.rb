class CreateTeamMembers < ActiveRecord::Migration
  def change
    create_table :team_members do |t|
t.string   :engagement_number , :limit=>10  # エンゲージメント番号
t.string   :employee_name , :limit=>40  # 従業員名
t.string   :user_id , :limit=>12  # ユーザーID
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

      t.integer :lock_version, :default => 0
#      t.timestamps
    end
  end
end
