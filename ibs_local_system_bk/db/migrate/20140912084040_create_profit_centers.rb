class CreateProfitCenters < ActiveRecord::Migration
  def change
    create_table :profit_centers do |t|
t.text   :profit_center   # 利益センタ
t.text   :profit_center_name_long   # 利益センタ名称Long
t.text   :profit_center_name_short    # 利益センタ名称Short
t.text   :pc_name_long    # 利益センタ名称ローカルLong
t.text   :pc_name_short   # 利益センタ名称ローカルShort
t.datetime   :updated_at    # レコード更新日時
t.datetime   :created_at    # レコード作成日時
t.datetime   :deleted_at    # レコード削除日時

      t.integer :lock_version, :default => 0
#     t.timestamps
    end
  end
end
