# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140919014430) do

  create_table "activity_types", force: true do |t|
    t.string   "company_code",  limit: 4
    t.text     "company_name"
    t.string   "staff_level",   limit: 4
    t.string   "profit_center", limit: 8
    t.decimal  "charge_rate",             precision: 17, scale: 0
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.integer  "lock_version",                                     default: 0
  end

  create_table "alert_rules", force: true do |t|
    t.integer  "priority"
    t.integer  "relative_fiscal_end_month"
    t.decimal  "progress_rate",                       precision: 10, scale: 2
    t.string   "bg_color",                  limit: 7
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.integer  "lock_version",                                                 default: 0
  end

  create_table "class_actual_values", force: true do |t|
    t.string   "job_level_code",    limit: 4
    t.integer  "plan_version_id"
    t.date     "actual_year_month"
    t.decimal  "actual_hour",                 precision: 10, scale: 2
    t.datetime "updated_at"
    t.datetime "created_at"
    t.integer  "lock_version",                                         default: 0
  end

  create_table "companies", force: true do |t|
    t.string   "company_code",        limit: 4
    t.string   "legacy_company_code", limit: 4
    t.string   "company_name",        limit: 25
    t.text     "name_local_language"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.integer  "lock_version",                   default: 0
  end

  create_table "engagement_tests", force: true do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "engagements", force: true do |t|
    t.string   "engagement_number",              limit: 10
    t.string   "engagement_descr",               limit: 40
    t.string   "engagement_legacy_number",       limit: 35
    t.integer  "em_sap_personnel_number"
    t.string   "em_legacy_emplid",               limit: 12
    t.integer  "ep_sap_personnel_number"
    t.string   "ep_legacy_emplid",               limit: 12
    t.decimal  "erp_realization_rate",                      precision: 11, scale: 2
    t.string   "entity_number",                  limit: 10
    t.string   "profit_center",                  limit: 10
    t.string   "local_service_type",             limit: 18
    t.string   "local_service_type_descr",       limit: 40
    t.string   "company_code",                   limit: 4
    t.string   "engagement_status",              limit: 30
    t.string   "engagement_type",                limit: 4
    t.string   "profit_center_descr",            limit: 40
    t.string   "assistant_name",                 limit: 40
    t.integer  "assistant_sap_personnel_number"
    t.string   "assistant_legacy_emplid",        limit: 12
    t.string   "em_name",                        limit: 40
    t.string   "ep_name",                        limit: 40
    t.string   "engagement_type_descr",          limit: 20
    t.string   "entity_name",                    limit: 35
    t.string   "lead_engagement_number",         limit: 10
    t.date     "san_expirty_date"
    t.string   "confidential_engagment",         limit: 1
    t.date     "san_start_date"
    t.decimal  "contract_amount",                           precision: 17, scale: 0
    t.date     "fiscal_end_date"
    t.text     "contract_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.integer  "lock_version",                                                       default: 0
  end

  create_table "local_job_levels", force: true do |t|
    t.string   "job_level_code", limit: 4
    t.text     "job_level_text"
    t.string   "company_code",   limit: 4
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.integer  "lock_version",             default: 0
  end

  create_table "plan_values", force: true do |t|
    t.string   "job_level_code",  limit: 4
    t.integer  "plan_version_id"
    t.decimal  "plan_hour",                 precision: 10, scale: 2
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.integer  "lock_version",                                       default: 0
  end

  create_table "plan_versions", force: true do |t|
    t.datetime "plan_last_update_date"
    t.text     "plan_last_update_user_name"
    t.string   "engagement_number",          limit: 10
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.integer  "lock_version",                          default: 0
  end

  create_table "profit_centers", force: true do |t|
    t.text     "profit_center"
    t.text     "profit_center_name_long"
    t.text     "profit_center_name_short"
    t.text     "pc_name_long"
    t.text     "pc_name_short"
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.integer  "lock_version",             default: 0
  end

  create_table "team_members", force: true do |t|
    t.string   "engagement_number", limit: 10
    t.string   "employee_name",     limit: 40
    t.string   "user_id",           limit: 12
    t.datetime "updated_at"
    t.datetime "created_at"
    t.datetime "deleted_at"
    t.integer  "lock_version",                 default: 0
  end

end
