// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .



// Engagements CompanyCode選択時
function changeCompanyCode(selCompanyCode)
{
    selProfileCenterOption(selCompanyCode.value);
}

// Engagements ProfileCenter取得して入れなおす
function selProfileCenterOption(CompanyCode)
{
    //取得
	$.ajax({
		type: 'GET',
		url: '/ect/ajax_get_data/get_profile_center',
		datatype: 'json',
		data: {
		  company_code: CompanyCode
		},
		success: function(get_data){
			var ret_data = $.parseJSON(get_data);
			//フォームに埋め込み治す
			for(i in ret_data){
				document.form1.setProfileCenter.options[i] = new Option(ret_data[i].name, ret_data[i].value);
			}
		},
		error: function(){
			alert('error');
		}
	});
}


// Engagementsクリアボタン
function clearButton(){
	document.form1.selCompanyCode.selectedIndex   = 0;
	document.form1.setProfileCenter.selectedIndex = 0;
	document.form1.setEngagementCode.value = "";


	//一度リセットする
    len = document.form1.setProfileCenter.options.length;
    for (i=len-1; i>=0; i--){
        document.form1.setProfileCenter.options[i] = null;
    }
    document.form1.setProfileCenter.options[0] = new Option("(Profile Center)", 0);
}
