class Ect::AjaxGetDataController < ApplicationController

  #assets/javascripts/application.jsから呼ばれる
  #Engagement一覧画面（検索用）
  def get_profile_center

    #選んだコード
    company_code = params['company_code'].to_i

    #仮のパラメータ
    json = {}
    for i in 0..5 do
      json[i] = {}
      json[i]['value'] = i      #ID取得
      json[i]['name']  = "ProfileCenter=#{i.to_s}_CompanyCode=#{company_code.to_s}"   #表示する名前を入れておく
    end

    #to_jsonで変換する
    render :text => json.to_json, :type=> "text/plain"
    return
  end

end
