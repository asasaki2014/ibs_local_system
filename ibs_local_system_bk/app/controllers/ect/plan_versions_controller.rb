class Ect::PlanVersionsController < ApplicationController

  def index
    #index
    @engagement_id = params[:engagement_id].to_s
  end

  def class_plan_values
    #class_plan_values
    @engagement_id = params[:engagement_id].to_s
    @plan_version_id = params[:plan_version_id].to_s

    #更新できない
    @bk_flag = true

    #前のshowを使う
    render :template => "ect/class_plan_values/show"

  end

end
