class Ect::EngagementsController < ApplicationController
  before_action :set_engagement, only: [:show, :edit, :update, :destroy]

  # GET /engagements
  # GET /engagements.json
  def index
    #@engagements = Engagement.all
    @engagements = Engagement.page(params[:page]).per(3).order('id DESC')

    @total_count = @engagements.count

    #メモ(作業予定)
    #
    #(EngagementsController)
    #
    #  ・Searchをおした時
    #    => 下にリスト表示
    #
    #  (Search後)
    #  ・Exportをおした時
    #  Excelにリストを出力
    #
    #  ・EngagementNameのリンクをクリックした時に
    #  ClassPlanValuesControllerへ遷移する
    #
    # => (ClassPlanValuesController)
    #     ・Editを押した時は
    #     ClassPlanValues/Editに遷移
    #
    #      => ・Class Settingを押した時は
    #         LocalJobLevelsControllerに遷移
    #
    #     ・Version Listを押した時は
    #     PlanVersionsControllerへ遷移する
    #
    #     ・Engagement Detailをおした時は
    #     ひとつ戻ってEngagementsController/Showへ遷移する
    #

  end

  # GET /engagements/1
  # GET /engagements/1.json
  def show
  end

  # GET /engagements/new
  def new
    @engagement = Engagement.new
  end

  # GET /engagements/1/edit
  def edit
  end

  # POST /engagements
  # POST /engagements.json
  def create
    @engagement = Engagement.new(engagement_params)

    respond_to do |format|
      if @engagement.save
        #format.html { redirect_to @engagement, notice: 'Engagement was successfully created.' }
        #format.json { render :show, status: :created, location: @engagement }
        format.html { redirect_to engagement_engagements_url, notice: 'Engagement was successfully created.' }
        format.json { render :index, status: :created, location: @engagement }
      else
        format.html { render :new }
        format.json { render json: @engagement.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /engagements/1
  # PATCH/PUT /engagements/1.json
  def update
    respond_to do |format|
      if @engagement.update(engagement_params)
        #format.html { redirect_to @engagement, notice: 'Engagement was successfully updated.' }
        #format.json { render :show, status: :ok, location: @engagement }
        format.html { redirect_to engagement_engagements_url, notice: 'Engagement was successfully updated.' }
        format.json { render :index, status: :ok, location: @engagement }
      else
        format.html { render :edit }
        format.json { render json: @engagement.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /engagements/1
  # DELETE /engagements/1.json
  def destroy
    @engagement.destroy
    respond_to do |format|
      format.html { redirect_to engagement_engagements_url, notice: 'Engagement was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_engagement
      # @engagement = Engagement.find(params[:id])
      @engagement = Engagement.where(:engagement_number => params[:id]).order(:id => :desc)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def engagement_params
      params.require(:engagement).permit(:engagement_number, :engagement_descr, :engagement_legacy_number, :em_sap_personnel_number, :em_legacy_emplid, :ep_sap_personnel_number, :ep_legacy_emplid, :erp_realization_rate, :entity_number, :profit_center, :local_service_type, :local_service_type_descr, :company_code, :engagement_status, :engagement_type, :profit_center_descr, :assistant_name, :assistant_sap_personnel_number, :assistant_legacy_emplid, :em_name, :ep_name, :engagement_type_descr, :entity_name, :lead_engagement_number, :san_expirty_date, :confidential_engagment, :san_start_date, :contract_amount, :fiscal_end_date, :contract_type, :deleted_at, :created_at, :updated_at)
    end
end
