require 'test_helper'

class Engagement::TestsControllerTest < ActionController::TestCase
  setup do
    @engagement_test = engagement_tests(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:engagement_tests)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create engagement_test" do
    assert_difference('Engagement::Test.count') do
      post :create, engagement_test: {  }
    end

    assert_redirected_to engagement_test_path(assigns(:engagement_test))
  end

  test "should show engagement_test" do
    get :show, id: @engagement_test
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @engagement_test
    assert_response :success
  end

  test "should update engagement_test" do
    patch :update, id: @engagement_test, engagement_test: {  }
    assert_redirected_to engagement_test_path(assigns(:engagement_test))
  end

  test "should destroy engagement_test" do
    assert_difference('Engagement::Test.count', -1) do
      delete :destroy, id: @engagement_test
    end

    assert_redirected_to engagement_tests_path
  end
end
